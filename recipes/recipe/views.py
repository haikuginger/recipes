from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.views import generic
from django.db.models import Q
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
from rest_framework import permissions

from recipe.models import Recipe, UserProfile

from recipe.serializers import RecipeSerializer
from recipe.serializers import UserProfileSerializer, UserProfileStub


class RecipeView(generic.DetailView):
    """
    Gives us a view of a particular recipe
    """
    model = Recipe
    template_name = 'recipe/single_recipe.html'

    def get_queryset(self):
        return Recipe.objects.filter(
            Q(private=False) | Q(author__account__id=self.request.user.id)
        )


class RecipeListView(generic.ListView):
    """
    Gives us a view of all recipes with links to individual pages
    """
    model = Recipe
    template_name = 'recipe/recipe_list.html'

    def get_queryset(self):
        return Recipe.objects.filter(
            Q(private=False) | Q(author__account__id=self.request.user.id)
        ).order_by('-date_created')


def default_view(request):
    return HttpResponse('Hi there!')


class RecipePermissions(permissions.BasePermission):
    """
    Handle permissions for a particular recipe
    """

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        else:
            return request.user.is_authenticated()

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            if not obj.private or obj.author.account == request.user:
                return True
        else:
            if obj.author.account == request.user:
                return True


class SingleUser(generics.RetrieveUpdateAPIView):
    """
    View handles a single UserProfile. We can show it,
    or we can update it. We don't currently allow user
    creation via the API.
    """
    serializer_class = UserProfileSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        user = self.request.user
        return user.profile

    def get_object(self):
        return self.get_queryset()


class SingleRecipe(generics.RetrieveUpdateDestroyAPIView):
    """
    Handles a single recipe
    """

    serializer_class = RecipeSerializer
    permission_classes = (RecipePermissions,)
    queryset = Recipe.objects.all()


class AllRecipes(generics.ListCreateAPIView):
    """
    Handles recipe creation and listing
    """

    serializer_class = RecipeSerializer
    permission_classes = (RecipePermissions,)
    
    def get_queryset(self):
        return Recipe.objects.filter(
            Q(private=False) | Q(author__account__id=self.request.user.id)
            ).order_by('-date_created')
