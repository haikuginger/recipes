from django.contrib import admin

from .models import Recipe, Ingredient

class IngredientsInline(admin.TabularInline):
    model = Ingredient

class RecipeAdmin(admin.ModelAdmin):
    model = Recipe
    inlines = [IngredientsInline]
    list_display = ['title', 'author', 'time_to_make']


admin.site.register(Recipe, RecipeAdmin)