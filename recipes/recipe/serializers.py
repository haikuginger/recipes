from rest_framework import serializers

from recipe.models import Recipe, Ingredient, Allergen, UserProfile, Favorite

from django.contrib.auth.models import User


class UserProfileStub(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = ('id', 'username', 'starred')


class IngredientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ingredient
        fields = ('name', 'quantity')


class RecipeSerializer(serializers.ModelSerializer):

    author = UserProfileStub(required=False)
    ingredients = IngredientSerializer(many=True)

    class Meta:
        model = Recipe
        fields = (
            'id',
            'title',
            'author',
            'private',
            'ingredients',
            'time_to_make',
            'directions'
        )

    def create(self, validated_data):
        ingredients = validated_data.pop('ingredients')
        author = self.context['request'].user.profile
        recipe = Recipe.objects.create(author=author, **validated_data)
        if ingredients:
            for ingredient in ingredients:
                ing = Ingredient.objects.create(recipe=recipe, **ingredient)
        return recipe

    def update(self, instance, validated_data):
        update_ingredients = validated_data.pop('ingredients', None)
        if update_ingredients:
            update_ingredients = {each['name']:each['quantity'] for each in update_ingredients}
            ingredients = instance.ingredients
            for name, quant in update_ingredients.items():
                try:
                    existing = instance.ingredients.get(name=name)
                    existing.quantity = quant
                    existing.save()
                except Ingredient.DoesNotExist:
                    new = Ingredient.objects.create(recipe=instance, name=name, quantity=quant)
                    new.save()
        restricted = ['id', 'author']
        for attr in validated_data:
            if attr not in restricted:
                setattr(instance, attr, validated_data[attr])
        instance.save()
        return instance


class RecipeStub(serializers.ModelSerializer):

    author = UserProfileStub()

    class Meta:
        model = Recipe
        fields = ('id', 'title', 'author')


class AllergenSerializer(serializers.ModelSerializer):

    class Meta:
        model = Allergen
        fields = ('allergen', 'severity')

    def create(self, validated_data):
        user = self.context['request'].user.profile
        allergen = Allergen.objects.create(user=user, **validated_data)
        return allergen


class FavoriteSerializer(serializers.Serializer):

    recipeid = serializers.IntegerField()
    title = serializers.CharField(max_length=100, required=False)
    author = UserProfileStub(required=False)


class UserProfileSerializer(serializers.ModelSerializer):

    favorites = FavoriteSerializer(many=True)
    recipes = RecipeStub(many=True)
    allergens = AllergenSerializer(many=True)

    class Meta:
        model = UserProfile
        fields = ('id', 'username', 'starred', 'favorites', 'allergens', 'recipes')

    def update(self, instance, validated_data):
        print(validated_data)
        # Elements on the profile are static aside from the Allergens and Favorites lists
        if 'allergens' in validated_data:
            for allergen in validated_data.get('allergens'):
                try:
                    existing = instance.allergens.get(allergen=allergen['allergen'])
                    existing.severity = allergen.get('severity', None)
                    existing.save()
                except Allergen.DoesNotExist:
                    new = Allergen.objects.create(user=instance, **allergen)
                    new.save()
        if 'favorites' in validated_data:
            for fav in validated_data.get('favorites'):
                print(fav)
                try:
                    existing = instance.favorites.get(recipe__id=fav['recipeid'])
                except Favorite.DoesNotExist:
                    try:
                        recipe = Recipe.objects.get(id=fav['recipeid'])
                        favorite = Favorite.objects.create(user=instance, recipe=recipe, recipeid=recipe.id)
                        favorite.save()
                    except Recipe.DoesNotExist:
                        pass
        return instance

