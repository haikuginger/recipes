from django.conf.urls import url, include

from django.views.decorators.csrf import csrf_exempt

from . import views


INSERT_VIEW_HERE = views.default_view

urlpatterns = [
    url(r'^$', views.default_view, name='default'),
    url(r'^recipes$', csrf_exempt(views.RecipeListView.as_view()), name='recipe_list'),
    url(r'^recipes/(?P<pk>[0-9]+).json', csrf_exempt(views.SingleRecipe.as_view()), name='single_recipe_api'),
    url(r'^recipes/(?P<pk>[0-9]+)', csrf_exempt(views.RecipeView.as_view()), name='single_recipe'),
    url(r'^recipes.json$', csrf_exempt(views.AllRecipes.as_view()), name='recipe_list_api'),
    url(r'^account.json$', csrf_exempt(views.SingleUser.as_view()), name='account_api'),
    url(r'^account/favorites/(?P<recipe_id>[0-9]+).json', INSERT_VIEW_HERE, name='single_favorite_api')
]