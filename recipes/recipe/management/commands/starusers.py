from django.core.management.base import BaseCommand

from recipe.models import UserProfile

class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        for profile in UserProfile.objects.all():
            if profile.recipes.count() >= 10:
                profile.starred = True
                print('Starring user {}...'.format(profile.username))
            else:
                profile.starred = False
            profile.save()
