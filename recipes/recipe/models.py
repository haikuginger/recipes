from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    """
    Stores user-specific information without needing to
    add more stuff to the quite-adequate built-in User model
    """
    account = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    starred = models.BooleanField("Is the user starred by the admins?", default=False)

    @property
    def username(self):
        return self.account.username

    def allergen_names(self):
        return [x.allergen for x in self.allergens.all()]


class Recipe(models.Model):
    author = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='recipes')
    private = models.BooleanField("Is the recipe private to the author?", default=False)
    title = models.CharField("Name of the recipe", max_length=100)
    time_to_make = models.DurationField("How long the recipe takes to make")
    date_created = models.DateTimeField(auto_now_add=True)
    directions = models.TextField("Directions to make the recipe")

    def is_favorited_by(self, user):
        # Did the named user favorite this recipe?
        if user.favorites.filter(recipe__id=self.id):
            return True
        return False


class Favorite(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='+')
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='favorites')
    recipeid = models.IntegerField(null=True)

    @property
    def author(self):
        return self.recipe.author

    @property
    def title(self):
        return self.recipe.title
    

class Ingredient(models.Model):
    """
    Helps the Recipe class store an arbitrary number of
    ingredient/quantity pairings
    """
    name = models.CharField("Name of the ingredient", max_length=100)
    quantity = models.CharField("How much of the ingredient we need", max_length=100, null=True)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='ingredients')


class Allergen(models.Model):
    """
    Stores one of the allergens of a particular user's allergens
    """
    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='allergens')
    allergen = models.CharField("Allergen", max_length=20)
    NONE = 'class0'
    LOW = 'v class1'
    MODERATE = 'class2'
    HIGH = 'class3'
    VERYHIGH = 'class4'
    CHOICES = (
        (NONE, 'Class 0 / No allergy'),
        (LOW, 'Class 1 / Low level'),
        (MODERATE, 'Class 2 / Moderate level'),
        (HIGH, 'Class 3 / High level'),
        (VERYHIGH, 'Class 4+ / Very high level')
    )
    severity = models.CharField("Level of allergic response", max_length=6, choices=CHOICES, null=True)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(account=instance)

def delete_if_quantity_null(sender, instance, **kwargs):
    if instance.quantity is None:
        instance.delete()

def delete_if_severity_null(sender, instance, **kwargs):
    if instance.severity is None:
        instance.delete()

models.signals.post_save.connect(create_user_profile, sender=User)
models.signals.post_save.connect(delete_if_quantity_null, sender=Ingredient)
models.signals.post_save.connect(delete_if_severity_null, sender=Allergen)